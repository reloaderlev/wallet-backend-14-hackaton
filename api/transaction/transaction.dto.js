export class TransactionDto {
  get composeList() {
    return this._composeList.bind(this);
  }

  _composeList(transactionsList) {
    return {
      transactionsList: transactionsList.map(
        trans => this.composeTransaction(trans).transaction,
      ),
    };
  }

  composeTransaction(transaction) {
    if (!transaction) {
      return null;
    }
    const transactionToReturn = transaction._doc;
    delete transactionToReturn.__v;
    return {
      transaction: { ...transactionToReturn },
    };
  }
}

export const transactionDto = new TransactionDto();
