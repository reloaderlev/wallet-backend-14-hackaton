/* eslint-disable no-magic-numbers */
/* eslint-disable require-await */
import { UserBalance } from '../user_balance/user_balance.model';

export class TransactionDao {
  static async getList(userId) {
    return this.find({ userId });
  }

  static async createOne(transaction) {
    const { userId, amount, type } = transaction;
    let balanceObj = await UserBalance.getBalanceByUserId(userId);

    if (!balanceObj) balanceObj = await UserBalance.createBalance(userId);

    const { _id, balance } = balanceObj;

    const balanceAfter =
      type === 'income' ? balance + amount : balance - amount;
    transaction.balanceAfter = balanceAfter;
    transaction.balanceAfterSign = balanceAfter >= 0 ? '+' : '-';

    await UserBalance.setBalanceById(_id, balanceAfter);

    return this.create({ ...transaction });
  }

  static async deleteTransaction(userId, transactionId, inc) {
    const { _id } = await UserBalance.getBalanceByUserId(userId);
    await UserBalance.updateOne({ _id }, { $inc: { balance: inc } });
    return this.deleteOne({ _id: transactionId });
  }

  static async collectStats(userId) {
    const statsAggregate = await this.aggregate()
      .match({ userId })
      .group({ _id: '$category', amount: { $sum: '$amount' } });

    return statsAggregate.reduce((acc, stat) => {
      acc[stat._id] = stat.amount;

      return acc;
    }, {});
  }
}
