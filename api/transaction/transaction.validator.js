import { AbstractValidator } from '../helpers/AbstractValidator';
import Validator from 'node-validator';

export class TransactionValidator extends AbstractValidator {
  constructor() {
    super();
  }

  get createTransaction() {
    return this._createTransaction.bind(this)();
  }

  _createTransaction() {
    const transactionRules = Validator.isObject()
      .withRequired('type', Validator.isString({ regex: /income|expense/ }))
      .withRequired('amount', Validator.isNumber())
      .withRequired('transactionDate', Validator.isDate())
      .withOptional(
        'category',
        Validator.isString({
          regex: /Food|Car|Self-care|Home-care|Children|Hobbies|Education|Other|Income/,
        }),
      )
      .withOptional('comment', Validator.isString());

    return this.runMiddleware.bind(this, transactionRules, 'body');
  }
}
