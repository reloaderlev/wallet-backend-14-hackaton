/* eslint-disable no-magic-numbers */
import mongoose, { Schema } from 'mongoose';
import { TransactionDao } from './transaction.dao';

const transactionSchema = new Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    type: {
      type: String,
      default: 'income',
      required: true,
    },
    transactionDate: {
      type: Date,
      required: true,
    },
    amount: {
      type: Number,
      default: 0,
      required: true,
    },
    category: {
      type: String,
      default: 'income',
      required: true,
    },
    comment: String,
    balanceAfter: {
      type: Number,
      required: true,
    },
    balanceAfterSign: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

transactionSchema.virtual('user', {
  ref: 'User',
  localField: 'userId',
  foreignField: '_id',
  justOne: false,
});

transactionSchema.loadClass(TransactionDao);

export const Transaction = mongoose.model('Transaction', transactionSchema);
