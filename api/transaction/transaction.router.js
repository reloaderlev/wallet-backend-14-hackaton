import { TransactionController } from './transaction.controller';
import { TransactionValidator } from './transaction.validator';
import { Router } from 'express';
import { authMiddleware } from '../auth/auth.middleware';

export class TransactionRouter {
  static initRouter() {
    const transController = new TransactionController();
    const transValidator = new TransactionValidator();
    const router = Router();

    router.get(
      '/:userId',
      authMiddleware.authorize,
      transController.getUserTransactions,
    );
    router.post(
      '/',
      authMiddleware.authorize,
      transValidator.createTransaction,
      transController.createTransaction,
    );
    router.delete(
      '/:transactionId',
      authMiddleware.authorize,
      transController.removeTransaction,
    );

    return router;
  }
}

export const transactionRouter = TransactionRouter.initRouter();
