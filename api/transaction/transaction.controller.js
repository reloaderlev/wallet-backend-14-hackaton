/* eslint-disable eqeqeq */
/* eslint-disable no-magic-numbers */
/* eslint-disable id-length */
import { CREATED, NO_CONTENT } from 'http-status-codes';
import { transactionDto } from './transaction.dto';
const { composeTransaction, composeList } = transactionDto;
import { Transaction } from './transaction.model';
import { ForbiddenError } from '../helpers/errorsConstructors';

export class TransactionController {
  get getUserTransactions() {
    return this._getUserTransactions.bind(this);
  }
  get createTransaction() {
    return this._createTransaction.bind(this);
  }
  get removeTransaction() {
    return this._removeTransaction.bind(this);
  }

  async _getUserTransactions(req, res, next) {
    try {
      const { userId } = req.params;
      const transactionsList = await Transaction.getList(userId);

      return res.status(CREATED).json(composeList(transactionsList));
    } catch (err) {
      next(err);
    }
  }

  async _createTransaction(req, res, next) {
    try {
      const transaction = req.body;
      const createdTransaction = await Transaction.createOne({
        ...transaction,
        userId: req.user._id,
      });

      res.status(CREATED).json(composeTransaction(createdTransaction));
    } catch (err) {
      next(err);
    }
  }

  async _removeTransaction(req, res, next) {
    try {
      const { transactionId } = req.params;

      const { userId, amount, type } = await Transaction.findOne({
        _id: transactionId,
      });
      const inc = type === 'Income' ? -amount : +amount;

      if (req.user._id.toString() !== userId.toString()) {
        throw new ForbiddenError('Transaction does not belong to user');
      }

      await Transaction.deleteTransaction(userId, transactionId, inc);

      return res.status(NO_CONTENT).send();
    } catch (err) {
      next(err);
    }
  }
}
