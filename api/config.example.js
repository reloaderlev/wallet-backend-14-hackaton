module.exports = {
  port: 3000,
  homepage: '',
  mongodb: {
    prodUrl: '',
    devUrl: '',
  },
  swagger: {
    host: 'localhost:3000',
    schemes: ['http'],
  },

  googleOAuth2: {
    clientID: '',
    clientSecret: '',
    callbackURL: 'http://localhost:3000/api/auth/google/callback',
  },
};
