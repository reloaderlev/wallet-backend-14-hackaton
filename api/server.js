import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { promisify } from 'util';
import mongoose from 'mongoose';
import config from './config';
import { exampleRouter } from './example/example.router';
import { authRouter } from './auth/auth.router';
import { userRouter } from './user/user.router';
import { balanceRouter } from './user_balance/user_balance.router';
import { transactionRouter } from './transaction/transaction.router';
import { transactionStatRouter } from './transactionStat/transactionStat.router';
import { passportStrategies } from './auth/passport.strategies';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './docs/index';
import passport from 'passport';
import cookieParser from 'cookie-parser';

export class WalletServer {
  constructor() {
    this.app = express();
    this.config = config;
  }

  async start() {
    this.initMiddlewares();
    await this.initDb();
    this.initRoutes();

    this.startListening();
  }

  initMiddlewares() {
    this.app.use(bodyParser.json());
    this.app.use(passport.initialize());
    this.app.use(cookieParser());
    this.app.use(cors('*'));
    passportStrategies.initStrategies();
  }

  initRoutes() {
    this.app.use(express.static('public'));
    this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    this.app.use('/api/examples', exampleRouter);
    this.app.use('/api/auth', authRouter);
    this.app.use('/api/users', userRouter);
    this.app.use('/api/transactions', transactionRouter);
    this.app.use('/api/user_balance', balanceRouter);
    this.app.use('/api/stats', transactionStatRouter);
  }

  async initDb() {
    const { prodUrl, devUrl } = this.config.mongodb;
    const dbUrl = process.env.NODE_ENV === 'production' ? prodUrl : devUrl;
    const connectPromise = promisify(mongoose.connect.bind(mongoose));

    try {
      await connectPromise(dbUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      console.log('Connected to mongodb with url', dbUrl);
    } catch (err) {
      console.log('Failed to connect to mongodb', err);
    }
  }

  async clearDb() {
    try {
      await mongoose.connection.db.dropDatabase();
      console.log('Cleared up database');
    } catch (err) {
      console.log('Error occured when tried to clear MongoDB', err);
    }
  }

  startListening() {
    this.server = this.app.listen(process.env.PORT || this.config.port, () => {
      console.log('Server started on port', this.config.port);
    });
  }
}
