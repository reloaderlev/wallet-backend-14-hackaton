import { AbstractValidator } from '../helpers/AbstractValidator';
import Validator from 'node-validator';

export class AuthValidator extends AbstractValidator {
  constructor() {
    super();
  }

  get signIn() {
    return this._signIn.bind(this)();
  }

  _signIn() {
    const signInRules = Validator.isObject()
      .withRequired('email', Validator.isString({ regex: /@/ }))
      .withRequired('password', Validator.isString());

    return this.runMiddleware.bind(this, signInRules, 'body');
  }

  get signUp() {
    return this._signUp.bind(this)();
  }

  _signUp() {
    const signUpRules = Validator.isObject()
      .withRequired('email', Validator.isString({ regex: /@/ }))
      .withRequired('password', Validator.isString())
      .withOptional('name', Validator.isString());

    return this.runMiddleware.bind(this, signUpRules, 'body');
  }
}
