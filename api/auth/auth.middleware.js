import { UnauthorizedError } from '../helpers/errorsConstructors';
import { sessionModel } from '../session/session.model';

export class AuthMiddleware {
  get authorize() {
    return this._authorize.bind(this);
  }

  async _authorize(req, res, next) {
    try {
      const authHeader = req.header('Authorization') || '';
      const token = authHeader.replace('Bearer ', '');

      if (!token) {
        throw new UnauthorizedError('Auth token not provided');
      }

      const session = await sessionModel.findSessionByToken(token);
      if (!session) {
        throw new UnauthorizedError('Auth token is not valid');
      }

      req.user = session.user;
      req.token = session.token;
      next();
    } catch (err) {
      next(err);
    }
  }
}

export const authMiddleware = new AuthMiddleware();
