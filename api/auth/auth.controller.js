import { CREATED, MOVED_TEMPORARILY, NO_CONTENT } from 'http-status-codes';
import { authDto } from './auth.dto';
const { composeAuthUser } = authDto;
import { sessionModel } from '../session/session.model';
import { userModel } from '../user/user.model';
import { encrypter } from '../helpers/encrypter';
import randomstring from 'randomstring';
import { ConflictError } from '../helpers/errorsConstructors';
import { homepage } from '../config';

export class AuthController {
  get signIn() {
    return this._signIn.bind(this);
  }
  get signInGoogle() {
    return this._signInGoogle.bind(this);
  }
  get signUp() {
    return this._signUp.bind(this);
  }
  get signOut() {
    return this._signOut.bind(this);
  }

  async _signIn(req, res, next) {
    try {
      const user = req.user;

      const session = await sessionModel.createSession(
        user,
        await this._createToken(user),
      );

      return res.status(CREATED).json(composeAuthUser(user, session));
    } catch (err) {
      next(err);
    }
  }

  async _signUp(req, res, next) {
    try {
      const { email, name, password } = req.body;
      const existingUser = await userModel.findUserByEmail(email);

      if (existingUser) {
        throw new ConflictError('User already exists!');
      }

      const newUser = await userModel.createUser({
        email,
        password,
        name,
      });
      const session = await sessionModel.createSession(
        newUser,
        await this._createToken(newUser),
      );

      res.status(CREATED).json(composeAuthUser(newUser, session));
    } catch (err) {
      next(err);
    }
  }

  _signInGoogle(req, res) {
    const { session } = req.user;
    return res.redirect(
      MOVED_TEMPORARILY,
      `${homepage}?token=${encodeURIComponent(session.token)}`,
    );
  }

  async _signOut(req, res, next) {
    try {
      await sessionModel.disableSession(req.token);
      return res.status(NO_CONTENT).send();
    } catch (err) {
      next(err);
    }
  }

  _createToken(user) {
    return encrypter.generateHash(
      user._id + randomstring.generate(this.tokenSaltLength),
    );
  }
}
