/* eslint-disable no-magic-numbers, require-await */
import bcrypt from 'bcryptjs';

export class Encrypter {
  constructor() {
    this.saltRounds = 5;
  }

  async generatePasswordHash(userPassword, salt) {
    return bcrypt.hash(userPassword, salt);
  }

  async comparePasswordHash(user, password) {
    return bcrypt.compare(password, user.passwordHash);
  }

  async generateHash(textToHash) {
    return bcrypt.hash(textToHash, this.saltRounds);
  }

  async generateSalt() {
    return bcrypt.genSalt(this.saltRounds);
  }
}

export const encrypter = new Encrypter();
