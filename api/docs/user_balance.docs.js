export const transactionEndpoints = {
  '/user_balance': {
    get: {
      tags: ['balance'],
      summary: 'Get user balance',
      description: '',
      operationId: 'getUserBalance',
      parameters: [
        {
          in: 'body',
          name: 'Credentials',
          description: 'User credentials for transaction',
          required: true,
          schema: {
            $ref: '#/definitions/Credentials',
          },
        },
      ],
      responses: {
        '400': {
          description: 'User request body format is invalid',
        },
        '401': {
          description: 'User provided wrong credentials',
        },
        '201': {
          description: 'Transaction added',
          schema: {
            $ref: '#/definitions/UserBalance',
          },
        },
      },
    },
  },
};

export const balanceDefinitions = {
  UserBalance: {
    type: 'object',
    properties: {
      userId: {
        type: 'ObjectId',
        $ref: '#/definitions/User',
      },
      balance: {
        type: 'number',
      },
    },
  },
};
