export const transactionStatEndpoints = {
  '/stats': {
    get: {
      tags: ['stats'],
      summary: 'Get transactions stats for logged user',
      description: '',
      operationId: 'getStats',
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          description: 'User authorization token with "Bearer " prefix',
          required: true,
          type: 'string',
        },
      ],
      responses: {
        '401': {
          description: 'Token is not valid or disabled',
        },
        '200': {
          description: 'Return transactions stats',
          schema: {
            $ref: '#/definitions/TransactionsStats',
          },
        },
      },
    },
  },
};

// eslint-disable-next-line id-length
export const transactionStatDefinitions = {
  TransactionsStats: {
    type: 'object',
    properties: {
      stats: {
        type: 'object',
        properties: {
          main: {
            type: 'number',
          },
          food: {
            type: 'number',
          },
          vehicle: {
            type: 'number',
          },
          development: {
            type: 'number',
          },
          children: {
            type: 'number',
          },
          home: {
            type: 'number',
          },
          education: {
            type: 'number',
          },
          other: {
            type: 'number',
          },
          income: {
            type: 'number',
          },
        },
      },
    },
  },
};
