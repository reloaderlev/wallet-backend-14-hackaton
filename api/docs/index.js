import { exampleEndpoints, exampleDefinitions } from './example.docs';
import { authDefinitions, authEndpoints } from './auth.docs';
import { transactionEndpoints, transactionDefinitions } from './transaction.docs';
import { swagger } from '../config';
import { userDefinitions, userEndpoints } from './user.docs';
import {
  transactionStatDefinitions,
  transactionStatEndpoints,
} from './transactionStat.docs';

export default {
  swagger: '2.0',
  info: {
    description: '',
    version: '1.0.0',
    title: 'Wallet Docs',
    contact: {
      email: 'mykola.levkiv@gmail.com',
    },
  },
  host: swagger.host,
  basePath: '/api',
  tags: [
    {
      name: 'example',
      description: 'example router',
    },
  ],
  schemes: swagger.schemes,
  paths: {
    ...exampleEndpoints,
    ...authEndpoints,
    ...userEndpoints,
    ...transactionEndpoints,
    ...transactionStatEndpoints,
  },
  definitions: {
    ...exampleDefinitions,
    ...authDefinitions,
    ...userDefinitions,
    ...transactionDefinitions,
    ...transactionStatDefinitions,
  },
};
