/* eslint-disable id-length */
export const transactionEndpoints = {
  '/transactions': {
    post: {
      tags: ['transactions'],
      summary: 'Create user transaction',
      description: '',
      operationId: 'createTransaction',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'body',
          name: 'TranssactionRequest',
          description: 'User credentials for transaction',
          required: true,
          schema: {
            $ref: '#/definitions/Transaction',
          },
        },
      ],
      responses: {
        '400': {
          description: 'User request body format is invalid',
        },
        '401': {
          description: 'User provided wrong credentials',
        },
        '201': {
          description: 'Transaction added',
          schema: {
            $ref: '#/definitions/Transaction',
          },
        },
      },
    },
  },
  '/transactions/user_id': {
    get: {
      tags: ['transaction'],
      summary: 'Gives user transactions list',
      description: '',
      operationId: 'getTransactions',
      parameters: [
        {
          in: '/{user_id}', 
        },
      ],
      responses: {
        '201': {
          description: 'User got transactions list',
          schema: {
            $ref: '#/definitions/Transaction',
          },
        },
      },
    },
  },
  '/transactions/transaction_id': {
    delete: {
      tags: ['transaction'],
      summary: 'Delete user transaction by id',
      description: '',
      operationId: 'deleteTransaction',
      parameters: [
        {
          in: '/{transaction_id}',
        },
      ],
      responses: {
        '201': {
          description: 'Transaction deleted',
          schema: {
            $ref: '#/definitions/Transaction',
          },
        },
        '403': {
          description: 'Transaction does not belong to user',
        },
      },
    },
  },
};

export const transactionDefinitions = {
  TransactionRequest: {
    type: 'object',
    properties: {
      userId: {
        type: 'ObjectId',
        $ref: '#/definitions/User',
      },
      type: {
        type: 'string',
      },
      transactionDate: {
        type: 'date',
      },
      amount: {
        type: 'number',
      },
      category: {
        type: 'string',
      },
      comment: {
        type: 'string',
      },
    },
  },
  Transaction: {
    type: 'object',
    properties: {
      userId: {
        type: 'ObjectId',
        $ref: '#/definitions/User',
      },
      type: {
        type: 'string',
      },
      transactionDate: {
        type: 'date',
      },
      amount: {
        type: 'number',
      },
      category: {
        type: 'string',
      },
      comment: {
        type: 'string',
      },
      balanceAfter: {
        type: 'namber',
      },
      balanceAfterSign: {
        type: 'string',
      },
    },
    xml: {
      name: 'Transaction',
    },
  },
};
