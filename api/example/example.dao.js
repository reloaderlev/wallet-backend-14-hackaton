export class ExampleDao {
  static findExample() {
    return this.findOne();
  }

  static createExample(textField) {
    return this.create({ textField });
  }
}
