import { NotFoundError } from '../helpers/errorsConstructors';
import { OK, CREATED } from 'http-status-codes';
import { exampleModel } from './example.model';
import { exampleDto } from './example.dto';
const { composeExample } = exampleDto;

export class ExampleController {
  get getExample() {
    return this._getExample.bind(this);
  }
  get createExample() {
    return this._createExample.bind(this);
  }

  async _getExample(req, res, next) {
    try {
      const example = await exampleModel.findExample();
      if (!example) {
        throw new NotFoundError('There is no examples yet');
      }

      return res.status(OK).json(composeExample(example));
    } catch (err) {
      next(err);
    }
  }

  async _createExample(req, res, next) {
    try {
      const { textField } = req.body;
      const example = await exampleModel.createExample(textField);

      return res.status(CREATED).json(composeExample(example));
    } catch (err) {
      next(err);
    }
  }
}
