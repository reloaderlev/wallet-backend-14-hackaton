// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { WalletServer } = require('../server');
const request = require('supertest-promised');
const { NOT_FOUND, OK } = require('http-status-codes');
const { exampleModel } = require('./example.model');

describe('Example API', () => {
  let server;
  let walletServer;

  before(async () => {
    walletServer = new WalletServer();
    await walletServer.start();
    await walletServer.clearDb();
    server = walletServer.server;
  });

  after(() => {
    server.close();
  });

  describe('GET /', () => {
    context('when there is no examples', () => {
      it('should throw 404 error', async () => {
        await request(server)
          .get('/api/examples')
          .expect(NOT_FOUND)
          .end();
      });
    });

    context('when there is at least one example', () => {
      const expectedTextField = 'someTextField';

      beforeEach(async () => {
        await exampleModel.create({
          textField: expectedTextField,
        });
      });

      afterEach(async () => {
        await walletServer.clearDb();
      });

      it('should return successful response', async () => {
        const responseBody = await request(server)
          .get('/api/examples')
          .expect(OK)
          .end()
          .get('body');

        responseBody.textField.should.be.eql(expectedTextField);
      });
    });
  });
});
