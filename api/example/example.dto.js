export class ExampleDto {
  composeExample(entity) {
    const { _id, textField } = entity;
    return {
      id: _id,
      textField,
    };
  }
}

export const exampleDto = new ExampleDto();
