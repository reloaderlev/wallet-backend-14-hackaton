import mongoose, { Schema } from 'mongoose';
import { ExampleDao } from './example.dao';

const exampleSchema = new Schema({
  textField: { type: String, required: true },
});

exampleSchema.loadClass(ExampleDao);
export const exampleModel = mongoose.model('Example', exampleSchema);

