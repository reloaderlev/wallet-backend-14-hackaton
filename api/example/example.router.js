import { ExampleController } from './example.controller';
import { ExampleValidator } from './example.validator';
import { Router } from 'express';

export class ExampleRouter {
  static initRouter() {
    const exampleController = new ExampleController();
    const exampleValidator = new ExampleValidator();
    const router = Router();

    router.get('/', exampleController.getExample);
    router.post(
      '/',
      exampleValidator.createExample,
      exampleController.createExample,
    );

    return router;
  }
}

export const exampleRouter = ExampleRouter.initRouter();
