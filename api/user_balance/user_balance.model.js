/* eslint-disable no-magic-numbers */
import mongoose, { Schema } from 'mongoose';
import { UserBalanceDao } from './user_balance.dao';

const userBalanceSchema = new Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
    },
    balance: Number,
  },
  {
    timestamps: true,
  },
);

userBalanceSchema.virtual('user', {
  ref: 'User',
  localField: 'userId',
  foreignField: '_id',
  justOne: false,
});

userBalanceSchema.loadClass(UserBalanceDao);
export const UserBalance = mongoose.model('UserBalance', userBalanceSchema);
