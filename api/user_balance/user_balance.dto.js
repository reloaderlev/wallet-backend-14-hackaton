export class BalanceDto {

  composeBalance({ userId, balance }) {
    return {
      balance: {
        userId,
        balance,
      },
    };
  }
}

export const balanceDto = new BalanceDto();
