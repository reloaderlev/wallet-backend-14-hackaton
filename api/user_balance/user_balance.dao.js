/* eslint-disable require-await */
export class UserBalanceDao {
  static getBalanceByUserId(userId) {
    return this.findOne({ userId });
  }

  static setBalanceById(_id, balance) {
    return this.updateOne(
      { _id },
      { balance },
    );
  }

  static createBalance(userId) {
    return this.create({userId, balance: 0});
  }
}
