/* eslint-disable no-magic-numbers */
import { OK } from 'http-status-codes';
import { balanceDto } from './user_balance.dto';
const { composeBalance } = balanceDto;
import { UserBalance } from './user_balance.model';

export class BalanceController {
  get getUserBalance() {
    return this._getUserBalance.bind(this);
  }

  async _getUserBalance(req, res, next) {
    try {
      const { userId } = req.params;
      let balance = await UserBalance.getBalanceByUserId(userId);
      if (!balance) balance = { userId, balance: 0 };

      return res.status(OK).json(composeBalance(balance));
    } catch (err) {
      next(err);
    }
  }
}
