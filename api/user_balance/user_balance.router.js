import { BalanceController } from './user_balance.controller';
import { Router } from 'express';
import { authMiddleware } from '../auth/auth.middleware';

export class BalanceRouter {
  static initRouter() {
    const balanceController = new BalanceController();
    const router = Router();

    router.get(
      '/:userId',
      authMiddleware.authorize,
      balanceController.getUserBalance,
    );

    return router;
  }
}

export const balanceRouter = BalanceRouter.initRouter();
