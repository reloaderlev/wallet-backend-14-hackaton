/* eslint-disable require-await */
import { encrypter } from '../helpers/encrypter';
import { userModel } from './user.model';

export class UserDao {
  static findUserByEmail(email) {
    return this.findOne({ email });
  }

  static findByEmailOrCreate(email, name) {
    return this.findOneAndUpdate(
      { email },
      { name },
      { new: true, upsert: true },
    );
  }

  async isPasswordCorrect(password) {
    const correct = await encrypter.comparePasswordHash(this, password);
    return correct;
  }

  static async createUser({ email, password, name }) {
    const passwordSalt = await encrypter.generateSalt();
    const passwordHash = await encrypter.generatePasswordHash(
      password,
      passwordSalt,
    );
    const user = await userModel({
      name,
      email,
      passwordHash,
      passwordSalt,
    }).save();

    return user;
  }
}
