/* eslint-disable require-await */
import { sessionStatuses } from './session.statuses';

export class SessionDao {
  constructor() {
    this.tokenSaltLength = 16;
  }

  static async createSession(user, token) {
    return this.create({
      userId: user._id,
      token,
    });
  }

  static async findSessionByToken(token) {
    return this.findOne({ token, status: sessionStatuses.ACTIVE }).populate(
      'user',
    );
  }

  static async disableSession(token) {
    return this.updateOne(
      {
        token,
      },
      {
        status: sessionStatuses.DISABLED,
      },
    );
  }
}
