// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { WalletServer } = require('../server');
const request = require('supertest-promised');
const { OK } = require('http-status-codes');
const { userModel } = require('../user/user.model');
const { sessionModel } = require('../session/session.model');
const { Transaction } = require('../transaction/transaction.model');

describe('TransactionStat API', () => {
  let server;
  let walletServer;

  before(async () => {
    walletServer = new WalletServer();
    await walletServer.start();
    await walletServer.clearDb();
    server = walletServer.server;
  });

  after(() => {
    server.close();
  });

  describe('GET /', () => {
    context('when there is no transactions', () => {
      const rightToken = 'valid_token';
      const expectedResponse = {
        stats: {
          Food: 0,
          Car: 0,
          'Self-care': 0,
          'Home-care': 0,
          Children: 0,
          Hobbies: 0,
          Education: 0,
          Other: 0,
          Income: 0,
        },
      };

      beforeEach(async () => {
        const user = await userModel.create({
          email: 'test@email.com',
        });
        await sessionModel.create({
          userId: user._id,
          token: rightToken,
        });
      });

      afterEach(async () => {
        await walletServer.clearDb();
      });

      it('should return successful response with zeros', async () => {
        const responseBody = await request(server)
          .get('/api/stats')
          .set('Authorization', `Bearer ${rightToken}`)
          .expect(OK)
          .end()
          .get('body');

        responseBody.should.be.deepEqual(expectedResponse);
      });
    });

    context('when user have transactions', () => {
      const rightToken = 'valid_token';
      const expectedResponse = {
        stats: {
          Food: 0,
          Car: 46,
          'Self-care': 0,
          'Home-care': 9,
          Children: 0,
          Hobbies: 0,
          Education: 0,
          Other: 100,
          Income: 12,
        },
      };

      beforeEach(async () => {
        const { _id: userId } = await userModel.create({
          email: 'test@email.com',
        });
        await sessionModel.create({
          userId,
          token: rightToken,
        });

        await Transaction.create({
          userId,
          type: 'income',
          transactonDate: new Date(),
          amount: 12,
          category: 'Income',
          balanceAfter: 0,
          balanceAfterSign: '-',
        });
        await Transaction.create({
          userId,
          type: 'cost',
          transactonDate: new Date(),
          amount: 15,
          category: 'Car',
          balanceAfter: 0,
          balanceAfterSign: '-',
        });
        await Transaction.create({
          userId,
          type: 'cost',
          transactonDate: new Date(),
          amount: 31,
          category: 'Car',
          balanceAfter: 0,
          balanceAfterSign: '-',
        });
        await Transaction.create({
          userId,
          type: 'cost',
          transactonDate: new Date(),
          amount: 9,
          category: 'Home-care',
          balanceAfter: 0,
          balanceAfterSign: '-',
        });
        await Transaction.create({
          userId,
          type: 'cost',
          transactonDate: new Date(),
          amount: 100,
          category: 'Other',
          balanceAfter: 0,
          balanceAfterSign: '-',
        });
      });

      afterEach(async () => {
        await walletServer.clearDb();
      });

      it('should return successful response with zeros', async () => {
        const responseBody = await request(server)
          .get('/api/stats')
          .set('Authorization', `Bearer ${rightToken}`)
          .expect(OK)
          .end()
          .get('body');

        responseBody.should.be.deepEqual(expectedResponse);
      });
    });
  });
});
