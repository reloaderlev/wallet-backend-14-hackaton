// import { ExampleController } from './example.controller';
import { TransactionStatController } from './transactionStat.controller';
import { Router } from 'express';
import { authMiddleware } from '../auth/auth.middleware';

export class TransactionStatRouter {
  static initRouter() {
    const transactionStatController = new TransactionStatController();
    const router = Router();

    router.get(
      '/',
      authMiddleware.authorize,
      transactionStatController.getStats,
    );

    return router;
  }
}

export const transactionStatRouter = TransactionStatRouter.initRouter();
