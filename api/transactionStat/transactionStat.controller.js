import { OK } from 'http-status-codes';
import { Transaction } from '../transaction/transaction.model';
import { transactionStatDto } from './transactionStat.dto';
const { composeStats } = transactionStatDto;

export class TransactionStatController {
  get getStats() {
    return this._getStats.bind(this);
  }

  async _getStats(req, res, next) {
    try {
      const { _id: userId } = req.user;

      const transactionsStats = await Transaction.collectStats(userId);

      return res.status(OK).json(composeStats(transactionsStats));
    } catch (err) {
      next(err);
    }
  }
}
