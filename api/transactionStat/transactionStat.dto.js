export class TransactionStatDto {
  composeStats(stats) {
    return {
      stats: [
        'Food',
        'Car',
        'Self-care',
        'Home-care',
        'Children',
        'Hobbies',
        'Education',
        'Other',
        'Income',
      ].reduce((acc, key) => {
        const NO_TRANSACTIONS_AMOUNT = 0;
        acc[key] = stats[key] || NO_TRANSACTIONS_AMOUNT;
        return acc;
      }, {}),
    };
  }
}

export const transactionStatDto = new TransactionStatDto();
