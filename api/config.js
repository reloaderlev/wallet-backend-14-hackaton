module.exports = {
  port: 3000,
  homepage: 'https://app-wallet-14.herokuapp.com/#/login',
  mongodb: {
    prodUrl:
      'mongodb+srv://levkiv:1234567890@testcluster-oqqlz.mongodb.net/wallet_14_hackaton?retryWrites=true&w=majority',
    devUrl:
      'mongodb+srv://levkiv:1234567890@testcluster-oqqlz.mongodb.net/wallet_14_hackaton?retryWrites=true&w=majority',
  },
  swagger: {
    host: 'app-wallet-14.herokuapp.com',
    schemes: ['https'],
  },

  googleOAuth2: {
    clientID:
      '194925151278-d6h2rooa5gjbkgrddf1solfh8qrvdiuk.apps.googleusercontent.com',
    clientSecret: 'Hro7feAnMdViDaAB8RMgtfBC',
    callbackURL: 'https://app-wallet-14.herokuapp.com/api/auth/google/callback',
  },
};
